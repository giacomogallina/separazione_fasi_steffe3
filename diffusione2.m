function diffusione2(n)
		%DIFFUSIONE_CALORE 

		% Discretizzazione del dominio nelle x e nelle y.
		x = linspace(0, 1, n);
		y = x;

		% Passo di discretizzazione spaziale
		h = x(2) - x(1);

		% Coefficiente di diffusione per il nostro operatore laplaciano
		K = 1e-2;

		% Step di integrazione in tempo
		dT = 0.01;

		% Calcoliamo quanti step mi servono ad arrivare a t = 5.
		nt = round(5 / dT);

		% La sorgente di calore è attiva solo fino a tempo 1/2, in un angolo del
		% dominio. 
		f = @(x,y,t) (t < .5) .* (x < .3) .* (x > .1) .* (y < .9) .* (y > .7);

		% Valutiamo il dato iniziale, nel nostro caso una campana esponenziale
		% centrata in mezzo al dominio; per questo ci serve una griglia di punti
		% che può essere generata con il comando meshgrid. 
		[xx, yy] = meshgrid(x, y);

		u0 = @(x,y) exp(-100*((x-.5).^2 + (y-.5).^2));

		U = u0(xx, yy);

		mesh(x, y, U);

		% Costruiamo l'operatore Laplaciano come una matrice sparsa, per rendere i
		% conti sufficientemente veloci. Questo è di vitale importanza!
		A0 = spdiags(ones(n, 1) * [1 -2 1], -1:1, n, n);
		A0(1, 1) = -1;
		A0(end, end) = -1;
		A = K * A0 / h^2;
		AA = kron(speye(n), A) + kron(A, speye(n));

		% Cominciamo ad integrare il sistema usando Eulero implicito
		for j = 1 : nt
				% Risolvo il sistema (I - dT * L) u_{n+1} = u_n + dT * f + dT * BC,
				% dove f è il termine sorgente    
				rhs = U + dT * f(xx, yy, j*dT);

				Unew = (speye(size(AA)) - dT * AA) \ reshape(rhs, (n)^2, 1);

				U = reshape(Unew, n, n);

				% Plot della soluzione
				mesh(x, y, U);
				title(sprintf('Time = %f', j * dT));
				axis([0 1 0 1 0 1])
				drawnow;
				pause(0.1);
		end



end

